import pandas as pd
import numpy as np
from validations import data_pipeline
import operator
import re


class ConditionalValidation(data_pipeline.DataPipeline):

    def calculate(self):
        conditional_validation_errors = []

        invalid_cases = pd.DataFrame([i for i in self.column_validator(self.condition_check, self.parameters)])

        for index, row in invalid_cases.iterrows():

            expected_data_format = [row['expected_data_format']]*len(row['row_id'])
            error_type = [row['error_type']] * len(row['row_id'])

            cv_error_df = pd.DataFrame({
                                        'excel_row': row['row_id'], 'error_entry': error_type,
                                        ' expected_data_format':  expected_data_format,
                                        })

            conditional_validation_errors.append(cv_error_df.to_json())

        print('conditional validation check complete ')

        return conditional_validation_errors if conditional_validation_errors else None

    def valid_values_for_column(self, column_name):
        return [x["enum"] for x in self.schema_all if str(x['to']) == column_name[0] and x['enum']]

    @staticmethod
    def enum_check(x, column_valid_cases):
        return np.logical_not(x in(column_valid_cases[0]))

    def empty_string(self,x):
        p = re.compile(' +')

        while p.match(str(x)) == None:
            return True
        else:
            return False

    @staticmethod
    def parameters(df, validation):

        expressions = validation['expressions']
        name = validation['name']

        if_statement = expressions.get('if')

        then_statement = expressions.get('then')
        operator_dict = {'<': operator.lt,'>': operator.gt,
                         '>=': operator.le, '==': operator.eq, '!=': operator.ne}
        if_cols = []
        for condition in if_statement:

            if_cols.append([col for col in df.columns if condition['column'] == col])

        then_cols = []
        for then_condition in then_statement:

            then_cols.append([col for col in df.columns if then_condition['column'] == col])

        return if_statement, then_statement, operator_dict, if_cols, then_cols, name

    def condition_check(self, df, col, conditional_dict, operator_dict):

        item = operator_dict.get(conditional_dict['operator'])

        if callable(item):

            if conditional_dict['value'] != '':
                column_dtype = df[col].dtype
                str_columns = [bool, object, str]
                if column_dtype in str_columns:
                    df[col] = df[col].astype(str)

                    # String-based comparison
                    error = df[col].apply(lambda x: item(x, conditional_dict['value']))

                elif column_dtype in [int, float]:
                    # Numeric comparison
                    error = df[col].apply(lambda x: item(x, np.float(conditional_dict['value'])))

                else:
                    # Left here for backward compatibility
                    error = df[col].apply(lambda x: item(str(x), conditional_dict['value']))

            else:
                if conditional_dict['operator'] == '==':
                    error = df[col].fillna('').apply(lambda x: x == '')
                else:
                    # must be operator '!=' as the other operators would not make sense with value {''}
                    error = df[col].fillna('').apply(lambda x: x != '')
            return error
        else:
            return None

    def column_condition_match(self, df, column_list, statement, string_ext, condition_check, operator_dict):
        for column in column_list:
            conditional_dict = next((condition for condition in statement if condition['column'] in column), None)
            if conditional_dict:
                column_name = column[0] + string_ext
                df[column_name] = condition_check(df, column[0], conditional_dict, operator_dict)
            else:
                continue
        return df

    def column_validator(self, condition_check, parameters):

        for validation in self.conditional_validations:
            df = self.df.copy()

            if_statement, then_statement, operator_dict, if_cols, then_cols, c_v_name = parameters(df, validation)

            self.column_condition_match(df, if_cols, if_statement, '_if_conditional', condition_check, operator_dict)
            self.column_condition_match(df, then_cols, then_statement, '_then_conditional', condition_check, operator_dict)

            if_cols_errors = [col for col in df.columns if 'if_conditional' in col]

            then_cols_errors = [col for col in df.columns if 'then_conditional' in col]

            df['if_errors'] = np.where(df[if_cols_errors].all(1), True, False)

            df['then_errors'] = np.where(df[then_cols_errors].all(1), True, False)

            df['error'] = np.where((df['if_errors'] == True) & (df['then_errors'] == False), True, False)

            if df['error'].any():
                sample = {
                          'row_id': list(self.df.loc[df.error == True]['row_index']),
                          'error_type': then_cols,
                          'expected_data_format': c_v_name
                          }

                yield sample


