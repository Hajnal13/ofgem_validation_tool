import pandas as pd
from validations import data_pipeline


class OutputGenerator(data_pipeline.DataPipeline):

    def json_csv_output(self, list_of_jsons):

        pd.set_option('display.max_colwidth', -1)
        error_json = pd.DataFrame(list_of_jsons).to_json()
        return error_json

    def output(self, ingestion_errors, data_validation_errors, business_validation_errors, conditional_validation_errors):

        if ingestion_errors:
            ingestion_errors_json = self.json_csv_output(ingestion_errors)
        else:
            ingestion_errors_json = None

        if data_validation_errors:
            data_type_validation_errors_json = self.json_csv_output(data_validation_errors)
        else:
            data_type_validation_errors_json = None

        if business_validation_errors:
            business_validation_errors_json = self.json_csv_output(business_validation_errors)
        else:
            business_validation_errors_json = None
        if conditional_validation_errors:
            conditional_validation_errors_json = self.json_csv_output(conditional_validation_errors)
        else:
            conditional_validation_errors_json = None
        validations = [i for i in [ingestion_errors_json, data_type_validation_errors_json,
                                   business_validation_errors_json, conditional_validation_errors_json] if i]
        return validations

