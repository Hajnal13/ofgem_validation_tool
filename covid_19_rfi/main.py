import re
import os
import xlrd
import webbrowser
import pandas as pd
import datetime as dt

from pathlib import Path
from jinja2 import Environment, FileSystemLoader
from covid_19_rfi import __version__ as version


def run_validation(input_dir_file, output_dir, export_csv):
    # Validation of arguments
    valid_path_args = validate_path_args(input_dir_file, output_dir)
    if not valid_path_args:
        return

    # Explicit boolean comparison to avoid non-emmpy values being considered as True.
    if export_csv != True:
        export_csv = False

    # Input path is either a directory or a file
    input_path = Path(input_dir_file)

    # Timestamp for log and filenames
    str_now = dt.datetime.strftime(dt.datetime.now(), "%Y%m%d_%H%M%S")

    # Output structure
    output_path = Path(output_dir)
    html_output_path = output_path / 'html_outputs'
    html_output_path.mkdir(exist_ok=True)

    if export_csv:
        output_path = output_path / 'valid_csvs' / 'csvs_{}'.format(str_now)
        output_path.mkdir(parents=True, exist_ok=True)

    # Validate all files
    results = validate_all_files(input_path, output_path, export_csv)

    # HTML results
    html_file = save_html_results(html_output_path, results, str_now)

    try:
        # Open the browser with the results
        webbrowser.open(html_file, new=2)
    except TypeError:
        #
        # Issue is documented here: https://bugs.python.org/issue30392
        print('To see the validation results, please open in a web browser the following file: file://{}'
              .format(html_file))


def validate_path_args(input_dir_file, output_dir):
    # Input validation
    input_path = Path(input_dir_file)

    # Input-dir-file validation
    if not input_path.exists():
        print("ERROR: Input directory or file does not exist. "
              "It is possible that the tool does not have read permissions at this path. "
              "Please check the argument --input-dir-file provided.")
        return False

    # Output-dir validation
    try:
        # Creates the output structure
        output_path = Path(output_dir)
        output_path.mkdir(parents=True, exist_ok=True)

    except PermissionError:
        print("ERROR: Output directory is invalid or this tool doesn't have write permissions to it. "
              "Please check the argument --output-dir provided.")
        return False

    # All validations passed
    return True


def validate_all_files(input_path, output_path, export_csv=False):
    # Temporary files to avoid during the validation process
    temporary_files_prefix = ['~', '$', '.']

    # Data templates
    templates_path = Path.cwd() / 'covid_19_rfi' / 'input_templates' / 'xlsx'

    # Regex patterns for filenames
    fn_pattern = r"^[^~$](.*?)\s*\_\s*weekly{}\s*\_\s*(0[1-9]|[1-2][0-9]|3[0-1])\s*\_\s*(0[1-9]|1[0-2])\s*\_\s*2020" \
                 r"\s*\.(xlsx|xls|csv)$"

    file_types = [{'id': 'part_a_qual',
                   'type': 'Part A Qualitative',
                   'template_path': templates_path / 'part_a_qualitative.xlsx',
                   'regex_pattern': re.compile(fn_pattern.format('consumerqualitative'), re.IGNORECASE)},
                  {'id': 'part_a_quant',
                   'type': 'Part A Quantitative',
                   'template_path': templates_path / 'part_a_quantitative.xlsx',
                   'regex_pattern': re.compile(fn_pattern.format('consumerquantitative'), re.IGNORECASE)},
                  {'id': 'part_b_qual',
                   'type': 'Part B Qualitative',
                   'template_path': templates_path / 'part_b_qualitative.xlsx',
                   'regex_pattern': re.compile(fn_pattern.format('financialqualitative'), re.IGNORECASE)},
                  {'id': 'part_b_quant',
                   'type': 'Part B Quantitative',
                   'template_path': templates_path / 'part_b_quantitative.xlsx',
                   'regex_pattern': re.compile(fn_pattern.format('financialquantitative'), re.IGNORECASE)},
                  # Old templates
                  {'id': 'old_part_b_qual',
                   'type': '--Old-- Part B Qualitative',
                   'template_path': templates_path / 'part_b_qualitative_old.xlsx',
                   'regex_pattern': re.compile(fn_pattern.format('financialqualitative'), re.IGNORECASE)}
                  ]

    for ft in file_types:
        # All template are read into a pandas dataframe for inferring misnamed files
        ft['df_template'] = pd.read_excel(ft['template_path'], dtype=str, na_filter=False, parse_dates=False,
                                          encoding='iso-8859-1')

        # Remove whitespaces to the left and to the right from strings
        ft['df_template'] = ft['df_template'].applymap(lambda x: x.strip() if isinstance(x, str) else x)

        # Changes all column names to lowercase to avoid case-sensitive issues
        ft['df_template'].rename(columns={c: c.lower() for c in ft['df_template'].columns}, inplace=True)

        # Forward fill for section column (to replace blanks with closest value above).
        # Only Template B Qualitative would require this, but it is applied to all as it may be useful.
        if 'section' in ft['df_template'].columns:
            ft['df_template']['section'] = ft['df_template']['section'].fillna(method='ffill')

    # Results structure
    all_results = []

    # Split directory or single file due to path handling
    if input_path.is_dir():
        input_path_str = str(input_path)
        len_input_path = len(input_path_str) + 1

        # Walk the input_dir including its sub-directories and files.
        for abs_path, dirs, files in os.walk(input_path_str):
            print("Checking {} ...".format(abs_path))
            dirs.sort()

            for file_name in sorted(files):
                # Exclude temporary files
                if file_name[0] in temporary_files_prefix:
                    continue

                print(" ... {}".format(file_name))
                relative_input_path = abs_path[len_input_path:]

                file_data = {'name': file_name,
                             'full_path': Path(abs_path) / file_name,
                             'output_dir': output_path / relative_input_path,
                             'name_type': '',
                             'content_type': '',
                             'errors': [],
                             'warnings': []}

                file_results = validate_file(file_data, file_types, export_csv)

                # List with all file results
                all_results.append(file_results)

    else:
        # A single file
        print("Checking {} ...".format(input_path))
        file_name = input_path.name

        if file_name[0] not in temporary_files_prefix:
            file_data = {'name': file_name,
                         'full_path': input_path,
                         'output_dir': output_path,
                         'name_type': '',
                         'content_type': '',
                         'errors': [],
                         'warnings': []}

            file_results = validate_file(file_data, file_types, export_csv)

            # List with all file results
            all_results.append(file_results)

    # `all_results` is a list of dictionaries containing lists of errors and warnings per file.
    return all_results


def validate_file(file_data, file_types, export_csv=False):

    # Extension check
    ext = Path(file_data['name']).suffix.upper()
    if ext not in ['.XLSX', '.XLS', '.CSV']:
        file_data['errors'].append('{} file type cannot be validated with this tool.'.format(ext))
        return file_data

    # Filename convention
    for ft in file_types:
        # Greedily checks for filename convention
        found_type = ft['regex_pattern'].match(file_data['name'])
        if found_type:
            file_data['name_type'] = ft['type']
            break

    if file_data['name_type'] == '':
        file_data['errors'].append('Filename convention is not correct. Please review the file naming convention.')

    # Date in filename must be a Monday between 1st February and today inclusive
    fn_date = None
    try:
        fn_parts = file_data['name'].split('_')
        fn_year = int(fn_parts[-1][:4])  # 2020
        fn_month = int(fn_parts[-2])
        fn_day = int(fn_parts[-3])

        fn_date = dt.date(fn_year, fn_month, fn_day)
        start_date = dt.date(2020, 2, 1)  # 1st February 2020
        end_date = dt.date.today()

        if fn_date.weekday() != 0 or not start_date <= fn_date <= end_date:  # 0: Monday
            file_data['errors'].append('Date in filename must be a Monday between 1st February 2020 and today.')

    except (ValueError, IndexError):
        file_data['errors'].append('Date in filename is invalid or could not be found.')

    ###
    # Read files, but it doesn't cast anything, everything is kept as strings as much as possible
    df_input = pd.DataFrame()
    input_encoding = 'iso-8859-1'

    if file_data['full_path'].suffix.lower() == '.csv':
        try:
            df_input = pd.read_csv(file_data['full_path'], dtype=str, na_filter=False, parse_dates=False,
                                   encoding=input_encoding)
            if len(df_input.columns) == 1:
                # It may be the case that the file uses tabs instead of commas, so it gives it a second try
                df_input = pd.read_csv(file_data['full_path'], sep='\t', dtype=str, na_filter=False, parse_dates=False,
                                       encoding=input_encoding)
        except pd.errors.ParserError:
            try:
                # This might be a CSV with tabs or there may be characters that need escaping.
                # Another try with tab separators
                df_input = pd.read_csv(file_data['full_path'], sep='\t', dtype=str, na_filter=False, parse_dates=False,
                                       encoding=input_encoding)
            except pd.errors.ParserError:
                try:
                    # This might be a CSV with semi-colons or there may be characters that need escaping.
                    # Another try with semi-colon separators
                    df_input = pd.read_csv(file_data['full_path'], sep=';', dtype=str, na_filter=False,
                                           parse_dates=False, encoding=input_encoding)
                except:
                    # If anything happens at this stage (after trying with several separators), it throws an error
                    file_data['errors'].append('CSV file could not be read; something seems to be wrong. \
                        As an alternative, please consider using your Excel files instead.')
                    # Nothing else to do
                    return file_data

    elif file_data['full_path'].suffix.lower() in ['.xlsx', '.xls']:
        try:
            df_input = pd.read_excel(file_data['full_path'], dtype=str, na_filter=False, parse_dates=False,
                                     encoding=input_encoding)
        except xlrd.biffh.XLRDError:
            file_data['errors'].append('File could not be opened. Please check that the file exists, \
                has read permissions or is not password protected.')
            # Nothing else to do
            return file_data

    # Remove whitespaces to the left and to the right from strings
    df_input = df_input.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    # Changes all column names to lowercase to avoid case-sensitive issues
    df_input.rename(columns={c: c.lower() for c in df_input.columns if isinstance(c, str)}, inplace=True)

    # Forward fill for section column (to replace blanks with closest value above).
    # Only Template B Qualitative would require this, but it is applied to all as it may be useful
    if 'section' in df_input.columns:
        df_input['section'] = df_input['section'].fillna(method='ffill')

    # Section, Question, and Unit must be present
    check_columns = ['section', 'question', 'unit']
    check_same_values = []
    for col in check_columns:
        if col in df_input.columns:
            check_same_values.append(col)
        else:
            file_data['errors'].append('{} column is missing.'.format(col[0].capitalize() + col[1:]))

    # Infers content-based template checking at the questions at row 1, if it doesn't match, then it match it by section
    df_template = pd.DataFrame()

    for field in ['question', 'section']:
        for ft in file_types:
            # Excludes old template from this check
            if ft['id'] == 'old_part_b_qual':
                continue

            try:
                # Checks first row field (question first, and then section if question didn't match)
                check = df_input[field].iloc[0] == ft['df_template'][field].iloc[0]
            except KeyError:
                # If the df_input doesn't have 'question' or 'section' respectively
                break

            # It greedily checks content type
            if check:
                file_data['content_type'] = ft['type']

                # Uses the template depending on the content as opposed to the filename
                df_template = ft['df_template']

                if file_data['name_type'] != '' and file_data['name_type'] != file_data['content_type']:
                    file_data['errors'].append('Content matches a different template (<b>{}</b>). \
                        Please correct its filename.'.format(file_data['content_type']))

                break

        if file_data['content_type'] != '':
            break

    # If content couldn't be inferred
    if file_data['content_type'] == '':
        file_data['errors'].append('File does not match any of the expected templates. Please review its contents.')
        # Nothing else to do
        return file_data

    ###
    # Schema checks
    ###

    # Number of columns must be the same as the number of columns in the template
    num_input_cols = len(df_input.columns)
    num_template_cols = len(df_template.columns)
    if num_input_cols < num_template_cols:
        file_data['errors'].append('File has fewer columns ({}) than those in the template ({}).'.format(
            num_input_cols,
            num_template_cols
        ))
    elif num_input_cols > num_template_cols:
        file_data['errors'].append('File has more columns ({}) than those in the template ({}).'.format(
            num_input_cols,
            num_template_cols
        ))
    else:
        # Same number of columns - lenghts must match to compare
        # Columns - check that the input file has the same columns as its template
        column_mismatch = df_input.columns != df_template.columns
        if column_mismatch.any() > 0:
            file_data['errors'].append('Column name(s) {} must be the same as template column name(s) {}.'.format(
                df_input.columns[column_mismatch].values,
                df_template.columns[column_mismatch].values)
            )

    # Number of rows must be the same as the number of rows in the template
    num_input_rows = len(df_input)
    num_template_rows = len(df_template)

    if num_input_rows < num_template_rows:
        file_data['errors'].append('File has fewer rows ({}) than those in the template ({}).'.format(
            num_input_rows,
            num_template_rows
        ))
    elif num_input_rows > num_template_rows:
        file_data['errors'].append('File has more rows ({}) than those in the template ({}).'.format(
            num_input_rows,
            num_template_rows
        ))

    # There must be a column name 'date'
    if 'date' not in df_input.columns:
        file_data['errors'].append('Date column is missing.')
    else:
        # Date must be in format DD/MM/YYYY and be the same as the one in the filename
        try:
            # Cast date column so that it is in the right format when the data frame is saved to CSV
            df_input['date'] = pd.to_datetime(df_input['date'], dayfirst=True, errors='ignore').dt.strftime('%d/%m/%Y')

            mismatch = df_input['date'] != fn_date.strftime('%d/%m/%Y')
            if mismatch.any() > 0:
                rows = [r + 2 for r in mismatch[mismatch].index.tolist()]  # to match Excel rows
                file_data['errors'].append('Date at row(s) {} must match date in filename.'.format(rows))

        except (AttributeError, ValueError):
            file_data['errors'].append('Date column has invalid dates or invalid format, and it must not be blank.')

    # Supplier
    if 'supplier' not in df_input.columns:
        file_data['errors'].append('Supplier column is missing.')
    else:
        # Supplier should not be nullable
        check = df_input['supplier'].str.len() < 1
        if check.any():
            rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
            file_data['errors'].append('Supplier at row(s) {} must not be blank.'.format(rows))

        # Supplier must be text with a maximum length
        supplier_max_len = 120
        check = df_input['supplier'].str.len() > supplier_max_len
        if check.any():
            rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
            file_data['errors'].append('Supplier at row(s) {} must not exceed {} characters.'
                                       .format(rows, supplier_max_len))

    # Checks if the old template is being used
    part_b_qual_new_template = False
    if file_data['content_type'] == 'Part B Qualitative':
        check = df_input['unit'] == 'GBP'
        if check.any():
            # part_b_qual_new_template = False
            # rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
            # file_data['errors'].append('<b>Old template</b> - '
            #                            'Please use the latest template for Part B Qualitative.'.format(rows))
            pass
        else:
            part_b_qual_new_template = True

    # This also applies for 'Part B Qualitative', but after it is confirmed that the new template is being used.
    if file_data['content_type'] in ['Part A Qualitative', 'Part A Quantitative', 'Part B Quantitative'] \
            or part_b_qual_new_template:

        # Number of rows must be the same to be able to compare
        if num_input_rows == num_template_rows:

            # Sections, Questions, and Units - check that the input file has exactly the same as its template
            for col_name in check_same_values:
                mismatch = df_input[col_name] != df_template[col_name]

                if mismatch.any() > 0:
                    rows = [r + 2 for r in mismatch[mismatch].index.tolist()]  # to match Excel rows
                    file_data['errors'].append('{} at row(s) {} must be the same as those in the template.'.format(
                        col_name[0].capitalize() + col_name[1:], rows))

    ###
    # Specific data-type validation per file
    ###
    if file_data['content_type'] == 'Part A Qualitative':
        # Check that relevant columns are not missing
        if 'comment' not in df_input.columns:
            file_data['errors'].append('Comment column is missing.')

    elif file_data['content_type'] == 'Part A Quantitative':
        # Check that relevant columns are not missing
        missing_col = 0
        if 'value' not in df_input.columns:
            file_data['errors'].append('Value column is missing.')
            missing_col += 1

        if 'unit' not in df_input.columns:
            # Already check above but still required here without an error message
            missing_col += 1

        if missing_col < 1:
            # Values in column `value` can be nullable/blank
            not_blank = df_input['value'] != ''
            df_view = df_input.loc[not_blank, ['unit', 'value']]

            if len(df_view) < 1:
                # All values are blank
                pass
            else:
                # Numeric: num, percent, GBP
                numeric_units = ['num', 'percent', 'GBP']
                df_numeric = df_view.loc[df_view['unit'].isin(numeric_units)]
                check = pd.to_numeric(df_numeric['value'], errors='coerce').isnull()
                if check.any():
                    rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                    file_data['errors'].append("Value at row(s) {} must be numeric. \
                    <b>Please avoid adding commas or non-numeric symbols, such as £, £k, £m, %, etc. \
                    If the question does not apply, \
                    please leave it BLANK (avoid adding N/A or comments to this cell)</b>.".format(rows))

                # Percent
                df_percent = df_view.loc[df_view['unit']=='percent']
                try:
                    percent_value = pd.to_numeric(df_percent['value'])
                    check = (percent_value < 0) | (percent_value > 100)
                    if check.any():
                        rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                        file_data['errors'].append('Value at row(s) {} must be a percent value between 0 and 100.'
                                                   .format(rows))

                    # Zero values do not require a warning as they are correct in both scales ([0,1] and [0,100])
                    percent_value = percent_value[percent_value != 0]
                    check = (percent_value >= 0) & (percent_value <= 1)
                    if check.all() and len(check) > 0:
                        rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                        file_data['warnings'].append("All percent values in this file, row(s) {}, are between 0 and 1. "
                                                     "This is potentially a mistake. Percent values for this part are "
                                                     "expected to be between 0 and 100. Please correct if the values "
                                                     "you meant are greater than 1 percent.".format(rows))

                except ValueError:
                    # In case there are non-numeric value and already accounted for above
                    pass

                # Text: Y/N or ''
                if df_input['value'].iloc[13] not in ['Y', 'y', 'N', 'n', '']:
                    file_data['errors'].append('Value at row 15 must be Y/N or blank.')

        # Comments
        if 'comment' not in df_input.columns:
            file_data['errors'].append('Comment column is missing.')

    elif file_data['content_type'] == 'Part B Qualitative':

        # Check that relevant columns are not missing
        missing_col = 0
        if 'response' not in df_input.columns:
            file_data['errors'].append('Response column is missing.')
            missing_col += 1

        # Unit is already checked above

    elif file_data['content_type'] == 'Part B Quantitative':

        # Check that relevant columns are not missing
        missing_col = 0
        if 'value' not in df_input.columns:
            file_data['errors'].append('Value column is missing.')
            missing_col += 1

        if 'unit' not in df_input.columns:
            # Already check above but still required here without an error message
            missing_col += 1

        if missing_col < 1:
            # Values in column `value` can be nullable/blank
            not_blank = df_input['value'] != ''
            df_view = df_input.loc[not_blank, ['unit', 'value']]

            if len(df_view) < 1:
                # All values are blank
                pass
            else:
                # Numeric: percent, GBP
                numeric_units = ['percent', 'GBP']
                df_numeric = df_view.loc[df_view['unit'].isin(numeric_units)]
                check = pd.to_numeric(df_numeric['value'], errors='coerce').isnull()
                if check.any():
                    rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                    file_data['errors'].append("Value at row(s) {} must be numeric. \
                    <b>Please avoid adding commas or non-numeric symbols, such as £, £k, £m, %, etc. \
                    If the question does not apply, \
                    please leave it BLANK (avoid adding N/A or comments to this cell)</b>.".format(rows))

                # Percent
                df_percent = df_view.loc[df_view['unit']=='percent']
                try:
                    percent_value = pd.to_numeric(df_percent['value'])
                    # Zero values do not require a warning as they are correct in both scales ([0,1] and [0,100])
                    percent_value = percent_value[percent_value != 0]
                    check = (percent_value >= -1) & (percent_value <= 1)
                    if check.all() and len(check) > 0:
                        rows = [r + 2 for r in check[check].index.tolist()]  # to match Excel rows
                        file_data['warnings'].append("All percent values in this file, row(s) {}, are between -1 and "
                                                     "1. This is potentially a mistake. Percent values for this part "
                                                     "are expected to be in a wider range (e.g., 70, -35). Please "
                                                     "correct if the values you meant should be in a wider scale."
                                                     .format(rows))
                except ValueError:
                    # In case there are non-numeric value and already accounted for above
                    pass

        # Comments
        if 'comment' not in df_input.columns:
            file_data['errors'].append('Comment column is missing.')

    ###
    # Save CSV file
    ###
    if export_csv and not len(file_data['errors']):
        filename = file_data['name']
        ext = Path(filename).suffix.lower()
        output_path = None

        if ext == '.xlsx':
            filename = filename.replace('.xlsx', '.csv')
            output_path = file_data['output_dir'] / 'csv_from_xlsx'

        elif ext == '.xls':
            filename = filename.replace('.xls', '.csv')
            output_path = file_data['output_dir'] / 'csv_from_xls'

        elif ext == '.csv':
            output_path = file_data['output_dir'] / 'csv_from_csv'

        if output_path:
            output_path.mkdir(parents=True, exist_ok=True)
            df_input.to_csv(output_path / filename, encoding='utf-8', index=False, date_format='%d/%m/%Y')

    return file_data


def save_html_results(html_output_path, results, str_now):
    # pathlib not supported by jinja2's Environment for the used version, hence the use of os.path
    # html_templates_path = Path.cwd() / "covid_19_rfi" / "output_templates"
    # env = Environment(loader=FileSystemLoader(html_templates_path))

    html_templates_dir = os.path.dirname(os.path.abspath(__file__))
    html_templates_dir = os.path.join(html_templates_dir, 'output_templates')
    env = Environment(loader=FileSystemLoader(html_templates_dir))

    template = env.get_template('validation_results_template.html')
    filename = html_output_path / 'validation_results_{}.html'.format(str_now)

    with open(filename, 'w') as fh:
        fh.write(template.render(
            h1="Hello Jinja2",
            show_one=True,
            show_two=False,
            version=version,
            validation_timestamp=str_now,
            results=results
        ))

    return filename
