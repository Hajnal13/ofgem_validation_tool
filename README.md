# Ofgem validation tool
### This tool reads a json schema that specifies columns within the data, data types, valid cases and conditional valid cases to check a csv data file for errors. 
#### For more background info see the Wiki pages:
#### 
*  **Use cases** for details on why this was created

*  **Validations** for more information on how these validations work 

*  **First timers** for users new to python
### v 1.0

**Getting started**

The user will need to have installed: 
- Python 3.6 
- An IDE or access to a terminal
- Git if contributing to the codebase
- Anaconda 3 or pip3 install -r requirements.txt

**Installation**

If running code:
- Download the code or git clone the code from the project ofgem_validation_tool

If contributing to the code:
- Create an SSH key
- Clone this repository to a folder on your local computer using git bash

**Code execution**

This code can be run in the terminal or using an IDE using the run.py file.

Navigate to the folder where your code is stored.

Three parameters are required for either method.

1) output_dir
2) json_file
3) file_path

If running from an IDE like spyder

output_dir:
- This is the folder where your output files will go
- Encase the path in single quotes and preface this with an r to enable the path
- to be read in raw string
- e.g. output_dir =r'C:\Users\your_area\output_folder'

json file:
- This is the json file containing the schema that is based off your data
- dictionary
- encase this file name in single quotes
- e.g. json_file = 'tariff.json'

file_path:
- This is the path that contains your data_files to be processed.
- Encase the path in single quotes and preface this with an r to enable the path to be read in raw string.
- Put asterisks known as wildcards where the folder name and file name change.
- e.g. 
- r'C:\User\Data Products\tariff_RFI\*\*.csv'

These three parameters need to be edited in the run.py file where default is written. 

To **run this code off dummy data and an example json from an IDE** change only the default parameter for the output folder 
so the output goes to somewhere on your computer. 

i.e. the first parameter in the file currently set 
as =**"C:\\Users\\usr\\project\\output_folder"**.

If running from the terminal:

-o:
- This is the folder where your output files will go. 
- Encase the path in single quotes

-j:
- This is the json file containing the schema that is used to validate your data

-f:
- This is the path that contains your data_files to be processed.
- Encase the path in single quotes
- Put asterisks known as wildcards where the folder name and filename change.

e.g. in one line: 
**python run.py -o'C:\Users\usr\project\output_folder' -j "unit_test.json" -f 'dummy_data\*.csv'**

To run this code off dummy data and an example json from the terminal, change only the parameter


-o'C:\Users\usr\project\output_folder' in the above command to a location on your computer 


**Outputs**

This validation tool produces three types of output.  

1) A CSV for each validation type, if these validation errors exist in the data
2) A JSON for each validation type, if these validation errors exist in the data
3) A single html output detailing all errors in data - the errors are truncated if exceeding a certain value
-- see output_generator.py to understand how the html output is created. An example is found in the directory 
html_example_output which can be opened directly in your browser.

The html is designed to be a user friendly report of errors in the data. 
The csvs and jsons have details of all errors in the submission

**Testing the code**

- A component test of the entire data pipeline can be run using python test_framework.py from the terminal or by running 
the test_framework.py file in your IDE.
- No parameters need to be specified as they are contained in the codebase of the ofgem_validation_tool
- The test json is already in the code in the directory jsons - 'unit_test.json' and a dummy data file in dummy_data 'unit_test.csv'.

**Contributing**

 - Fork the code
 - Create your feature branch: git checkout -b my-new-feature
 - Commit your changes: git commit -am 'Add some feature'
 - Push to the branch: git push origin my-new-feature
 - Submit a pull request
 - Maintainers of the code will review requests prior to merging
 
**Security**

- This code has been reviewed in alignment with Ofgem's disclosure process
- Maintainers will be reviewing any merge requests prior to approval. However, users 
download or clone the repository at their own risk.
- The data contained within this code 'unit_test.csv' are a synthesized dataset 
and not suitable for research or policy making.

**Maintainers**

Anushka Fernando

**Author**

Anushka Fernando

**Correspondance**


