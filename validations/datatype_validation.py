from validations.data_pipeline import DataPipeline
import numpy as np
import pandas as pd
import re
import datetime
from dateutil import parser


class DataTypeValidation(DataPipeline):

    def calculate(self):
        data_validation_errors = []

        invalid_cases = pd.DataFrame([i for i in self.data_type_and_nullable_check()])

        for index, row in invalid_cases.iterrows():

            error_column = [row['table_column']] * len(row['error_entry'])
            dict_error = [row['expected_data_format']]*len(row['error_entry'])
            error_entry = row['error_entry']
            excel_row = row['row_id']

            error_df = pd.DataFrame({'table_column':error_column, 'error_entry':error_entry,
                                     'excel_row':excel_row, 'expected datatype': dict_error,
                                     })

            data_validation_errors.append(error_df.to_json())

        print('data type validation complete')

        return data_validation_errors if data_validation_errors else None, self.file_name, self.no_of_customers

    def valid_dtypes_for_column(self, column_name):
        return [x["type_to"] for x in self.schema_all if str(x['to']) == column_name[0]]

    def num_check(self, x):
        return False if isinstance(x, (int, float)) else True

    def bool_check(self, x):
        return False if isinstance(x, (bool)) else True

    def dtype_check(self, x, python_dtype):
        return True if type(x) != python_dtype[0] else False

    def to_numeric_check(self, x):
        p = re.compile('\d+(\.\d+)?')
        while p.match(x)==None:
            return True
        else:
            return False

    def str_to_num_check(self,x, nullable_column):
        if not (self.is_nan_check(x) or x ==''):
            error = self.to_numeric_check(x)

        else:
            error = self.nullable_check(nullable_column)

        return error

    def nullable_check(self, nullable_column):
        return False if nullable_column[0]==True else True

    def is_nan_check(self, x):
            return x is np.nan or x != x

    def double(self, x, nullable_column):
        if not (self.is_nan_check(x) or x ==''):
            error = self.num_check(x)

        else:
            error = self.nullable_check(nullable_column)

        return error

    def alphanum(self, x, nullable_column):
        if not (self.is_nan_check(x) or x ==''):
            return False
        else:
            return self.nullable_check(nullable_column)

    def date_parser(self, x, nullable_column):

        if not (self.is_nan_check(x) or x ==''):
            date = str(x)
            if len(date) < 6:
                return True
            else:
                try:
                    parser.parse(date, dayfirst=True)
                    return False

                except ValueError:
                    try:
                        parser.parse(date)
                        return False

                    except ValueError:
                        try:
                            datetime.datetime.strptime(date, '%Y-%d-%b')
                            return False

                        except ValueError:
                            return True

        else:
            return self.nullable_check(nullable_column)

    def date_checker(self, x, nullable_column):
        error = self.date_parser(x, nullable_column)
        return error

    def python_data_type_check(self, x, python_dtype, nullable_column):
        if not (self.is_nan_check(x) or x ==''):
            error = self.dtype_check(x, python_dtype)
        else:
            error = self.nullable_check(nullable_column)
        return error

    def numerical_check(self,  column, nullable_column):

        numerical_datatypes = ['float64', 'int64']
        if self.df[column].dtypes in numerical_datatypes:

            self.df['error'] = self.df[column].apply(lambda x: self.double(x, nullable_column))

        elif self.df[column].dtypes == object:

            self.df['error'] = self.df[column].apply(lambda x: self.str_to_num_check(x, nullable_column))

        return self.df['error']

    def str_num_check(self,  column):

        numerical_datatypes = ['int64']

        if self.df[column].dtypes in numerical_datatypes:
            self.df['error'] = self.df[column].apply(lambda x: self.int_check(x))
        elif self.df[column].dtypes == 'object':
            self.df['error'] = ~self.df[column].str.isalpha()
        return self.df['error']

    def str_check(self, x, nullable_column):
        if not (self.is_nan_check(x) or x ==''):
            try:
                error = x.isdecimal()
            except AttributeError:
                error = False
        else:
            error = self.nullable_check(nullable_column)
        return error

    def str_null_check(self, column, nullable_column):
        self.df['error'] = self.df[column].apply(lambda x: self.str_check(x, nullable_column))
        return self.df['error']

    def date(self):
        self.df['error'] = False
        return self.df['error']

    def data_type(self, column, python_dtype, nullable_column):

        if self.df[column].isnull().any() == False:
            try:
                self.df[column] = self.df[column].astype(python_dtype[0])
            except Exception as ex:
                self.df['error'] = ex

        self.df['error'] = self.df[column].apply(
            lambda x: self.python_data_type_check(x, python_dtype, nullable_column))

        return self.df['error']

    def data_type_and_nullable_check(self):

        data_types = {'date': datetime, 'string': str, 'boolean': bool, 'int': int, 'double': 'num', 'misc': 'misc',
                      'alphanum': 'alphanum'}
        hr_datatypes = {'date': 'DD/MM/YYYY or YY MM DD HH:MM:SS', 'string': 'text', 'boolean': 'Y/N', 'int': 'number',
                        'double': 'number (max no. of decimal places is 6), see data dictionary', 'misc': 'text_or_number', 'alphanum':'expected string or text'}

        for column in self.df.columns:

            if len(column) > 1:

                data_column = column.replace(" ", "_").lower()

                dict_column = [i['to'] for i in self.schema_all if (data_column == i['to'])]
                nullable_columns = [i['to'] for i in self.schema_all if i["nullable"] == True]
                pattern = r'Date|date|TIMESTAMP'
                date_cols = [c for c in self.df.columns if re.search(pattern, c)]

                if not dict_column:

                    continue

                elif dict_column:

                    nullable_column = [True if dict_column[0] in nullable_columns else False]

                    column_dtype = self.valid_dtypes_for_column(dict_column)

                    python_dtype = [v for k, v in data_types.items() if column_dtype[0] == k]
                    hr_dtype = [v for k, v in hr_datatypes.items() if column_dtype[0] == k]

                    if column in date_cols:

                        self.df['error'] = self.df[column].apply(lambda x: self.date_checker(x, nullable_column))

                    elif column_dtype[0] == 'double':
                        self.df['error'] = self.numerical_check(column, nullable_column)

                    elif column_dtype[0] == 'misc':
                        self.df['error'] = self.str_num_check(column)

                    elif column_dtype[0] == 'alphanum':
                        self.df['error'] = self.df[column].apply(lambda x: self.alphanum(x, nullable_column))

                    elif column_dtype[0] == 'string':
                        self.df['error'] = self.str_null_check(column, nullable_column)

                    elif column_dtype[0] == 'boolean':
                        self.df['error'] = self.df[column].apply(lambda x: self.bool_check(x))
                    else:
                        self.df['error'] = self.data_type(column, python_dtype, nullable_column)

                    if self.df['error'].any():
                        sample = {'table_column': column,
                                  'error_entry': list(self.df.loc[self.df.error == True][column]),
                                  'row_id': list(self.df.loc[self.df.error == True]['row_index']),
                                  'error_type': column_dtype[0],
                                  'expected_data_format': hr_dtype[0]
                                  }

                        yield sample

        else:
            pass


if __name__ == "__main__":
    pass

# Data_pipeline = DataTypeValidation(DataPipeline)
# failed_rows = Data_pipeline.calculate()
# print([i for i in failed_rows])