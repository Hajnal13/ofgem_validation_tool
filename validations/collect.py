import pandas as pd
import os
import re


class Collect:

    def __init__(self, file_path):
        self.file_path = file_path

    def date_column_finder(self):
        try:
            df = pd.read_csv(self.file_path, encoding='utf8', na_filter=False)
        except UnicodeDecodeError:
            df = pd.read_csv(self.file_path, encoding='cp1252', na_filter=False)


        pattern = r'Date|date|TIMESTAMP'
        date_cols = [c for c in df.columns if re.search(pattern, c)]
        return date_cols

    def csv_reader(self):

        supplier_folder = os.path.split(self.file_path)

        file_name = os.path.basename(supplier_folder[1])

        file_type_split = os.path.split(supplier_folder[0])
        file_type = file_type_split[1]
        supplier = os.path.split(file_type_split[0])

        supplier_name = supplier[1]

        date_columns = self.date_column_finder()
        date_col_dict = {}
        for col in date_columns:
            date_col_dict[col] = str
        try:

            df = pd.read_csv(self.file_path, encoding='utf8', converters=date_col_dict, na_filter=False)
        except UnicodeDecodeError:
            df = pd.read_csv(self.file_path, encoding='cp1252', converters=date_col_dict, na_filter=False)

        no_of_customers = len(df)

        # FIXME: This is a workaround to be able to perform non case-sensitive comparisons for this data column.
        if 'tariff_advertised_name' in df.columns:
            df['tariff_advertised_name'] = df['tariff_advertised_name'].str.lower()
        ###

        # FIXME: This is ad-hoc logic used to remove whitespaces to the left and to the right from strings. \
        #  It is placed here temporarily until design decisions for it are made.
        df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)
        ###

        return df, supplier_name, file_name, file_type, no_of_customers


if __name__ == "__main__":
    pass
    # Data_pipeline = DataPipeline(output_dir, json_file, file_path)



