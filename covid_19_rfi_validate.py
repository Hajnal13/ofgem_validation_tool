import argparse
from covid_19_rfi import __version__ as version
from covid_19_rfi.main import run_validation


def run():
    parser = argparse.ArgumentParser(description='Covid-19 RFI Validation - Version {}'.format(version))

    # Input directory including all sub-contents
    parser.add_argument('-i', '--input-dir-file', type=str, metavar='', required=False,
                        # Example
                        # default=r"C:\\Users\\username\\Documents\\covid_validation\\inputs",
                        default=r"",
                        help='path to directory or single file to validate')

    # Output directory
    parser.add_argument('-o', '--output-dir', type=str, metavar='', required=False,
                        # Example
                        # default=r"C:\\Users\\username\\Documents\\covid_validation\\outputs",
                        default=r"",
                        help='path to directory for validation results')

    # Whether to export UTF-8 CSV files
    parser.add_argument('-x', '--export-csv', type=str, metavar='', required=False,
                        # Example
                        # default=False,
                        default=True,
                        help='whether to save CSV files after validation')

    args = parser.parse_args()

    input_dir_file = args.input_dir_file
    output_dir = args.output_dir
    export_csv = args.export_csv

    run_validation(input_dir_file, output_dir, export_csv)


if __name__ == '__main__':
    run()
