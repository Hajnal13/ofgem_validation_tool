import os
from jinja2 import Environment, FileSystemLoader
from pathlib import Path
import pandas as pd
from validations import data_pipeline


class OutputGenerator(data_pipeline.DataPipeline):

    def process(self, list_of_jsons):

            validation_errors = []
            for json_instance in list_of_jsons:

                df = pd.read_json(json_instance)
                pd.set_option('display.max_colwidth', -1)
                if len(df) == self.total_rows:
                    error_df = df.head(1)
                    error_df = error_df.assign(excel_row='ALL ROWS IN YOUR DATA HAVE ERRORS')
                    error_df = error_df.assign(error_entry='multiple errors')

                elif len(df) >=20:

                    error_df = df.head(1)
                    error_df = error_df.assign(excel_row=str(len(df)) + ' ROWS IN YOUR DATA HAVE ERRORS')
                    error_df = error_df.assign(error_entry='multiple errors')

                else:
                    error_df=df
                validation_errors.append(error_df.to_html())

            return validation_errors

    def html_output_generator(self, filename, ingestion_errors_html,  data_validation_errors_html, business_validation_errors_html, conditional_validation_errors_html):

        root = os.path.dirname(os.path.abspath(__file__))
        path = Path(self.output_dir)
        pd.set_option('display.max_colwidth', -1)

        templates_dir = os.path.join(root, 'templates')
        env = Environment(loader=FileSystemLoader(templates_dir))

        template = env.get_template('error_html_template.html')
        if not path /"html_outputs":
            os.makedirs

        html_output_path = path /"html_outputs"
        if not os.path.exists(html_output_path):
            os.makedirs(html_output_path)

        # filename = os.path.join(path + os.sep + "html_outputs", 'error_html_{}.html'.format(filename))
        filename = path /"html_outputs"/'errors_{}.html'.format(filename[:-4])

        with open(filename, 'w') as fh:
            fh.write(template.render(
                h1="Hello Jinja2",
                show_one=True,
                show_two=False,
                ingestion_timestamp=self.timestamp,
                missing_obligatory_columns=ingestion_errors_html,
                datatype_tables=data_validation_errors_html,
                bv_tables=business_validation_errors_html,
                cv_tables=conditional_validation_errors_html
            ))

    def html_error_free_output_generator(self, filename):

        root = os.path.dirname(os.path.abspath(__file__))
        path = Path(self.output_dir)
        pd.set_option('display.max_colwidth', -1)

        templates_dir = os.path.join(root, 'templates')
        env = Environment(loader=FileSystemLoader(templates_dir))

        template = env.get_template('no_error_html_template.html')
        if not path /"html_outputs":
            os.makedirs

        html_output_path = path /"html_outputs"
        if not os.path.exists(html_output_path):
            os.makedirs(html_output_path)

        # filename = os.path.join(path + os.sep + "html_outputs", 'error_html_{}.html'.format(filename))
        filename = path /"html_outputs"/'no_errors_{}.html'.format(filename[:-4])

        with open(filename, 'w') as fh:
            fh.write(template.render(
                h1="Hello Jinja2",
                show_one=True,
                show_two=False,
                ingestion_timestamp=self.timestamp,
            ))
        return

    def json_csv_output(self, list_of_jsons, error_type):
        path = self.error_log_path
        pd.set_option('display.max_colwidth', -1)
        output_csv = os.path.join(path, error_type + self.timestamp + "_{}.csv".format(self.file_name[:-4]))
        output_json = os.path.join(path, error_type + self.timestamp + "_{}.json".format(self.file_name[:-4]))

        pd.DataFrame(list_of_jsons).to_json(output_json)
        pd.DataFrame(list_of_jsons).to_csv(output_csv)

        return

    def output(self, filename,  ingestion_errors, data_validation_errors, business_validation_errors, conditional_validation_errors):
        if ingestion_errors:
            error_type = 'ingestion_errors_'
            self.json_csv_output(ingestion_errors, error_type)
            ingestion_errors_html = self.process(ingestion_errors)
        else:
            ingestion_errors_html = None

        if data_validation_errors:
            error_type = 'data_type_validation_errors_'
            self.json_csv_output(data_validation_errors, error_type)
            data_validation_errors_html = self.process(data_validation_errors)
        else:
            data_validation_errors_html = None
        if business_validation_errors:
            error_type = 'business_type_validation_errors_'
            self.json_csv_output(business_validation_errors, error_type)
            business_validation_errors_html = self.process(business_validation_errors)
        else:
            business_validation_errors_html = None
        if conditional_validation_errors:
            error_type = 'conditional_type_validation_errors_'
            self.json_csv_output(conditional_validation_errors, error_type)
            conditional_validation_errors_html = self.process(conditional_validation_errors)
        else:
            conditional_validation_errors_html = None

        self.html_output_generator(filename, ingestion_errors_html,  data_validation_errors_html, business_validation_errors_html, conditional_validation_errors_html)

        return

