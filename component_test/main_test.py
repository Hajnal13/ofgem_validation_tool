from validations import ingestion_validation
from validations import datatype_validation
from validations import business_validation
from validations import conditional_validation
from component_test import output_generator_test
import glob
import pandas as pd


def process_data(error_log_path, file_path, output_dir, json_file):

    for data_file in glob.iglob(file_path):

        Ingestion_validation_local_pipeline = ingestion_validation.IngestionValidation(error_log_path, output_dir, json_file, data_file)
        ingestion_errors = Ingestion_validation_local_pipeline.calculate()

        Data_type_validation_local_pipeline = datatype_validation.DataTypeValidation(error_log_path, output_dir, json_file, data_file)
        data_type_errors, filename, total_rows = Data_type_validation_local_pipeline.calculate()

        Business_validation_local_pipeline = business_validation.BusinessValidation(error_log_path, output_dir, json_file, data_file)
        business_type_errors = Business_validation_local_pipeline.calculate()

        Conditional_validation_local_pipeline = conditional_validation.ConditionalValidation(error_log_path, output_dir, json_file,data_file)
        conditional_type_errors = Conditional_validation_local_pipeline.calculate()

        validations = [pd.DataFrame(ingestion_errors).to_json(), pd.DataFrame(data_type_errors).to_json(),  pd.DataFrame(business_type_errors).to_json(), pd.DataFrame(conditional_type_errors).to_json()]

        if validations:
            Output_generator_local_pipeline = output_generator_test.OutputGenerator(error_log_path, output_dir,
                                                                                                 json_file, data_file)
            validations = Output_generator_local_pipeline.output(ingestion_errors, data_type_errors, business_type_errors, conditional_type_errors)

        print('all checks complete')

        return validations






